<%@ page import="pl.sda.store.Product" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: nesciotamen
  Date: 2019-07-23
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product List</title>
</head>
<body>
<%--<jsp:include page="/header.jsp"></jsp:include>--%>
<table>
    <tr>
        <th style="width: 150px;">Name</th>
        <th style="width: 150px;">Add product to inventory</th>
    </tr>
    <%
        List<Product> productList = (List<Product>) request.getAttribute("product");

        for (Product product : productList) {
            out.print("<tr>");
            out.print("<td>" + product.getName() + "</td>");
            out.print("<td>" + "<a href=\"/inventory/add?id=" + product.getId() + "\">" + "Add product to inventory</a>" + "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
