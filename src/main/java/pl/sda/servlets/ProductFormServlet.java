package pl.sda.servlets;

import pl.sda.database.EntityDao;
import pl.sda.store.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/product/add")
public class ProductFormServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/product/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String name = req.getParameter("name");
        Product product = new Product(name);

        entityDao.saveOrUpdate(product);

        resp.sendRedirect("/product/list");
    }
}
