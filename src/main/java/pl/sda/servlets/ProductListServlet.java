package pl.sda.servlets;

import pl.sda.database.EntityDao;
import pl.sda.store.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/product/list")
public class ProductListServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String id = req.getParameter("id");

        if (id != null) {
            final Optional<Product> optionalProduct = entityDao.findById(Product.class, Long.parseLong(id));
            if (optionalProduct.isPresent()) {
                req.setAttribute("product", optionalProduct.get().getInventoryList());
            } else {
                System.err.println("Not found this product");
            }
        } else {
            req.setAttribute("product", entityDao.findAll(Product.class));
        }

        req.getRequestDispatcher("/product/list.jsp").forward(req, resp);

    }
}
